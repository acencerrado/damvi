package misCosicasBonicas;

import java.util.Scanner;

public class miReloj {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Dime una hora");
		int h = sc.nextInt();

		System.out.println("Dime los minutos");
		int m = sc.nextInt();

		System.out.println("Dime los segundos");
		int s = sc.nextInt();

		System.out.println(h + ":" + m + ":" + s);

		System.out.println("Cuantos segundos quieres sumarle?");
		int segAñadidos = sc.nextInt();

		System.out.println("procesando...");

		s = s + segAñadidos;

		if (s >= 60) {
			m = (s / 60) + m;
			s = s % 60;

			if (m >= 60) {
				h = (m / 60) + h;
				m = m % 60;

			}
		}
		mostrar(h, m, s);
		//System.out.println(h + ":" + m + ":" + s);
		sc.close();
	}

	private static void mostrar(int h, int m, int s) {

		String sh = "" + h;
		String sm = "" + m;
		String ss = "" + s;

		if (h < 10) {
			sh = "0" + h;
		}
		if (m < 10) {
			sm = "0" + m;
		}
		if (s < 10) {
			ss = "0" + s;
		}

		System.out.println(sh + ":" + sm + ":" + ss);
	}
}
