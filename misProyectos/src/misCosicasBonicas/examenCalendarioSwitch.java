package misCosicasBonicas;

import java.util.Scanner;

public class examenCalendarioSwitch 
{
//
	public static void main(String[] args)
	{
		System.out.println("Dime un mes");
		Scanner sc = new Scanner(System.in);
		int mes = sc.nextInt();
		
		System.out.println("Dime un dia de mes");
		
		int dia = sc.nextInt();
		int diasMes = 0;
		
		
		switch (mes) 
		{
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			diasMes = 31;
			break;
		case 2:
			diasMes = 28;
			break;
		case 4:
		case 6:
		case 9:
		case 11:
			diasMes = 30;
			break;
		default:
			System.out.println("Error de mes!");
		}
		if (dia > diasMes) 
		{
			System.out.println("Error dia!");
		}else 
		{
			System.out.println("Faltan "+(diasMes-dia)+" dias para terminar el mes");
		}
		
	}
}
